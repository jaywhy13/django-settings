# -*- coding: utf-8 -*-
VERSION = '1.3-1-beta'

__version__ = VERSION
__author__ = "Kuba Janoszek"

# module public API
from .api import *

